package com.apps.anesabml.tomatish;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.anesabml.tomatish.data.TaskContract.TaskEntry;
import com.apps.anesabml.tomatish.data.TaskDbHelper;

public class MainActivity extends AppCompatActivity {

    private final String LOG_TAG = this.getClass().getSimpleName();

    /* Id for the pendingIntent */
    private final int INTENT_ID = 0;
    private final int NOTIFICATION_ID = 1;
    private MediaPlayer mMediaPlayer;
    private Notification.Builder mNotificationBuilder;
    private NotificationManager mManager;

    private static final String WAKE_LOCK_TAG = "wake lock";
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    private TextView mTimerTextView;
    private Button mStartButton, mCancelButton;
    private ProgressBar mProgressBar;
    private EditText mEditText;
    private SharedPreferences mSharedPrefs;

    private final long COUNT_DOWN_INTERVAL = 1000; // 1 second
    private long timeRemainingInMilli;
    private int focusTime, restTime;
    private boolean isRunning;


    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        int mSeconds = 0;
        int mMinutes = 0;

        @Override
        public void run() {
            if (timeRemainingInMilli > 0) {
                timeRemainingInMilli -= 1000;
                mSeconds = (int) timeRemainingInMilli / 1000;
                mProgressBar.setProgress(mSeconds);
                mMinutes = mSeconds / 60;
                mSeconds = mSeconds % 60;
                String updatedTimer = String.format("%d:%02d", mMinutes, mSeconds);
                mTimerTextView.setText(updatedTimer);
                mNotificationBuilder.setContentText(updatedTimer);
                mManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
                handler.postDelayed(this, COUNT_DOWN_INTERVAL);
            } else {
                handler.removeCallbacks(runnable);
                if (isRunning) {
                    mMediaPlayer.start();
                    timeRemainingInMilli = restTime * 1000;
                    handler.postDelayed(runnable, COUNT_DOWN_INTERVAL);
                    mProgressBar.setMax(restTime);
                    isRunning = false;
                } else {
                    mWakeLock.release();
                    mMediaPlayer.start();
                    mManager.cancelAll();
                    mStartButton.setText(R.string.start);
                    mTimerTextView.setText(focusTime / 60 + "");
                    mProgressBar.setProgress(focusTime);
                    addTaskToDatabase();
                }

            }
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hide the title and the elevation
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("");

        // Initialize the media player
        mMediaPlayer = MediaPlayer.create(this, R.raw.slow_spring_board);
        // Get the default SharedPreference
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String stringFocusTime = mSharedPrefs.getString(
                getString(R.string.work_time_key),
                getString(R.string.default_work_timer_in_minutes));
        focusTime = Integer.parseInt(stringFocusTime) * 60;

        String stringRestTime = mSharedPrefs.getString(
                getString(R.string.rest_time_key),
                getString(R.string.default_rest_timer_in_minutes));
        restTime = Integer.parseInt(stringRestTime) * 60;

        // Find the elements
        mTimerTextView = findViewById(R.id.timer);
        mStartButton = findViewById(R.id.start_button);
        mCancelButton = findViewById(R.id.cancel_button);
        mProgressBar = findViewById(R.id.progress_bar);
        mEditText = findViewById(R.id.tasks_edit_text);

        mTimerTextView.setText(stringFocusTime);
        mStartButton.setText(getString(R.string.start));
        mCancelButton.setText(R.string.cancel);

        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_TAG);

        // Create an Intent and a PendingIntent
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, INTENT_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Build a notification
        mNotificationBuilder = new Notification.Builder(this)
                .setContentTitle(getString(R.string.time_remaining))
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_notify);

        // Get the notification manager
        mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // On click listener for the start button that handle start and pause and resume actions
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                String string = button.getText().toString();
                if (string.equals(getString(R.string.start))) {
                    timeRemainingInMilli = focusTime * 1000;
                    mProgressBar.setMax(focusTime);
                    mProgressBar.setProgress(focusTime);
                    // Run the Runnable
                    handler.postDelayed(runnable, COUNT_DOWN_INTERVAL);
                    mWakeLock.acquire();
                    mMediaPlayer.start();
                    // Show the notification
                    mManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
                    // Set the text of the button to "PAUSE"
                    button.setText(R.string.pause);
                    mEditText.setEnabled(false);
                    isRunning = true;
                } else if (string.equals(getString(R.string.resume))) {
                    // Re run the Runnable if the user click on the resume button
                    handler.postDelayed(runnable, COUNT_DOWN_INTERVAL);
                    // Set the text of the button to "PAUSE"
                    button.setText(R.string.pause);
                } else { // if the user click on pause
                    // Remove the callback to stop the runnable
                    handler.removeCallbacks(runnable);
                    // Set the text of the the button to "RESUME"
                    button.setText(R.string.resume);
                }
            }
        });

        // On click listener for the cancel button
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWakeLock.isHeld()) {
                    // Clear the handler and the notification
                    handler.removeCallbacks(runnable);
                    mManager.cancel(NOTIFICATION_ID);
                    mWakeLock.release();
                    // Reset the layout items to the default state
                    mEditText.setEnabled(true);
                    mStartButton.setText(R.string.start);
                    mTimerTextView.setText("" + focusTime / 60);
                    mProgressBar.setProgress(focusTime);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String stringFocusTime = mSharedPrefs.getString(
                getString(R.string.work_time_key),
                getString(R.string.default_work_timer_in_minutes));
        focusTime = Integer.parseInt(stringFocusTime) * 60;
        mTimerTextView.setText(stringFocusTime);

        String stringRestTime = mSharedPrefs.getString(
                getString(R.string.rest_time_key),
                getString(R.string.default_rest_timer_in_minutes));
        restTime = Integer.parseInt(stringRestTime) * 60;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.settings:
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            case R.id.history:
                Intent historyIntent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(historyIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addTaskToDatabase(){
        String task = mEditText.getText().toString();
        if (TextUtils.isEmpty(task)){
            return;
        } else {
            TaskDbHelper dbHelper = new TaskDbHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(TaskEntry.COLUMN_NAME_TITLE, task);

            db.insert(TaskEntry.TABLE_NAME,null, values);
        }
    }
}
