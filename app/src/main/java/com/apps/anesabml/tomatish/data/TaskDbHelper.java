package com.apps.anesabml.tomatish.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apps.anesabml.tomatish.data.TaskContract.TaskEntry;
/**
 * Created by anesabml on 23/01/18.
 */

public class TaskDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME =  "Tasks.db";
    public static final int DATABASE_VERSION = 2;

    public TaskDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlCreateEntries =
                "CREATE TABLE " + TaskEntry.TABLE_NAME + "(" +
                        TaskEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        TaskEntry.COLUMN_NAME_TITLE + " TEXT);";
        db.execSQL(sqlCreateEntries);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            onCreate(db);
    }
}
