package com.apps.anesabml.tomatish;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.anesabml.tomatish.data.TaskContract.TaskEntry;
import com.apps.anesabml.tomatish.data.TaskDbHelper;

import java.util.ArrayList;


/**
 * Created by anesabml on 23/01/18.
 *
 * An Activity that shows the history of task.
 */

public class HistoryActivity extends AppCompatActivity {

    private ListView mListView;
    private TextView mEmptyTextView;
    private ArrayList<String> mList;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);

        mEmptyTextView = findViewById(R.id.empty_text);
        mEmptyTextView.setVisibility(View.INVISIBLE);

        mListView = findViewById(R.id.list);
        mList = new ArrayList<>();
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        mAdapter.addAll(retrieveHistoryFromDataBase());
        mListView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.clean:
                cleanHistory();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Helper Method to retrieve the data from the database
     * @return a list of tasks
     */
    private ArrayList<String> retrieveHistoryFromDataBase() {

        // Get a readable database
        TaskDbHelper dbHelper = new TaskDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                TaskEntry._ID,
                TaskEntry.COLUMN_NAME_TITLE};

        Cursor cursor = db.query(
                TaskEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        // Get the index of the column
        int titleColumnIndex = cursor.getColumnIndex(TaskEntry.COLUMN_NAME_TITLE);
        while (cursor.moveToNext()) {
            String task = cursor.getString(titleColumnIndex);
            mList.add(task);
        }
        if (mList.isEmpty()) {
            mEmptyTextView.setVisibility(View.VISIBLE);
        }
        return mList;
    }

    /**
     * Helper Method to clean the database
     */
    private void cleanHistory() {
        // Get a writable database
        TaskDbHelper dbHelper = new TaskDbHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Remove the hole table
        int rowsDeleted = db.delete(TaskEntry.TABLE_NAME,
                null,
                null);
        if (rowsDeleted != 0) {
            Toast.makeText(this, R.string.history_cleaned, Toast.LENGTH_SHORT).show();
        }
        mAdapter.clear();
        mEmptyTextView.setVisibility(View.VISIBLE);
    }
}
