package com.apps.anesabml.tomatish.data;

import android.provider.BaseColumns;

/**
 * Created by anesabml on 23/01/18.
 */

public final class TaskContract {

    private TaskContract(){}

    /** Class that define the table content */
    public static class TaskEntry implements BaseColumns {

        // ormlite
        public static final String _ID = BaseColumns._ID;
        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_NAME_TITLE = "title";
    }
}
